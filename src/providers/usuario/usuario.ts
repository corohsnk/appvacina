import { HttpClientProvider } from '../http-client/http-client';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
@Injectable()
export class UsuarioProvider {

  constructor(public http: HttpClientProvider) {
  }

  salvarUsuario(usuario) : Observable<any> {
    return this.http.post(this.http.url + 'usuario', usuario);
  }

  editarUsuario(usuario) : Observable<any> {
    return this.http.put(this.http.url + 'usuario', usuario);
  }

  logar(usuario) : Observable<any> {
    return this.http.get(this.http.url + 'usuario?email=' + usuario.email + '&senha=' + usuario.senha, {});
  }

}
