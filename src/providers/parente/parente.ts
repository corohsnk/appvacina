import { Injectable } from '@angular/core';
import { HttpClientProvider } from '../http-client/http-client';
import { Observable } from 'rxjs';

@Injectable()
export class ParenteProvider {

  constructor(public http: HttpClientProvider) {
    
  }

  getParentes(email) : Observable<any> {
    return this.http.get(this.http.url + "parente?email=" + email, {});
  }

  salvarParente(parente) : Observable<any> {
    return this.http.post(this.http.url + "parente", parente);
  }

  alterarParente(parente) : Observable<any> {
    return this.http.post(this.http.url + "parente", parente);
  }
}
