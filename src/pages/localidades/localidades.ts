import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegisterPage } from '../register/register';


declare var google: any;

@IonicPage()
@Component({
  selector: 'page-localidades',
  templateUrl: 'localidades.html',
})
export class LocalidadesPage {
  @ViewChild('map') mapRef: ElementRef;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {

    this.showMap();
  }


  showMap() {

    const location = new google.maps.LatLng(-23.5613793,-46.7402834);
   
    const options = {
      center: location,
      zoom: 10,
      streetViewControl:false,
      mapTypeId:'roadmap'
    };
    const map = new google.maps.Map(this.mapRef.nativeElement, options);
    this.addMarket(location,map)
  }


  addMarket(position,map){
    return new google.maps.Marker({
      position,
      map
    });
  }


  initMap() {
    var myLatlng = {lat: -25.363, lng: 131.044};

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 4,
      center: myLatlng
    });

    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'Click to zoom'
    });

    map.addListener('center_changed', function() {
      // 3 seconds after the center of the map has changed, pan back to the
      // marker.
      window.setTimeout(function() {
        map.panTo(marker.getPosition());
      }, 3000);
    });

    marker.addListener('click', function() {
      this.navCtrl.setRoot(RegisterPage);
    });
  }

}
