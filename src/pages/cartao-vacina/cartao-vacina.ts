import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CartaoVacinaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cartao-vacina',
  templateUrl: 'cartao-vacina.html',
})
export class CartaoVacinaPage {
  public vacinas: any[];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.vacinas = [
                    {Nome: "Gripe", Descricao: "BlaBlaBlaBla", Data: "30/12/1992", valido: true},
                    {Nome: "Sarampo", Descricao: "Hheheheh", Data: "30/12/2000", valido: false},
                    {Nome: "Tríplice Viral", Descricao: "Sarampo, caxumba e rubéola", Data: "30/12/2017", valido: true}
                  ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CartaoVacinaPage');
  }

}
