import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UsuarioProvider } from '../../providers/usuario/usuario';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  public formRegistrar;
  public loading: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public usuarioProvider: UsuarioProvider,
     public toastCtrl: ToastController, public loadingCtrl: LoadingController) {
    this.loading = false;
    this.formRegistrar = new FormGroup({
      nome_usuario: new FormControl("", Validators.compose([
        Validators.required
      ])),
      usuario_login: new FormControl("", Validators.compose([
        Validators.required        
      ])),
      senha: new FormControl("", Validators.compose([
        Validators.required        
      ])),
      data_nascimento: new FormControl("", Validators.compose([
        Validators.required,      
      ])),
      endereco: new FormControl("", Validators.compose([
        Validators.required        
      ])),
      end_numero: new FormControl("", Validators.compose([
        Validators.required        
      ])),
      cep: new FormControl("", Validators.compose([
        Validators.required        
      ])),
      complemento: new FormControl("", Validators.compose([
        Validators.required        
      ])),
      email: new FormControl("", Validators.compose([
        Validators.required        
      ]))
    })
  }

  registrar(usuario) {
    let showLoading = this.showLoading('Registrando...');
    showLoading.present();
    this.usuarioProvider.salvarUsuario(usuario)
    .subscribe( data => {
      showLoading.dismiss();
      this.navCtrl.setRoot(HomePage,  {}, {animate: true, direction: 'forward'});
    }, err => {
      showLoading.dismiss();
      let toast = this.toastCtrl.create({
        message: 'Erro ao se registrar no sistema.',
        duration: 2000,
        position: 'bottom'
      });
      toast.present();
    })
    
  }

  voltar() {
    this.navCtrl.setRoot(LoginPage, {}, {animate: true, direction: 'forward'});
  }

  private showLoading(message) {
    return this.loadingCtrl.create({
      content: message
    });
  }


}
