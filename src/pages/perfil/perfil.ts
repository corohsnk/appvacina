import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UsuarioProvider } from '../../providers/usuario/usuario';
@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {
  public formPerfil;
  constructor(public navCtrl: NavController, public navParams: NavParams, public usuarioProvider: UsuarioProvider,
    public toastCtrl: ToastController, public loadingCtrl: LoadingController) {
    const usuario = JSON.parse(localStorage.getItem('usuario'));
    this.formPerfil = new FormGroup({
      nome_usuario: new FormControl(usuario.nome_usuario, Validators.compose([
        Validators.required
      ])),
      usuario_login: new FormControl(usuario.usuario_login, Validators.compose([
        Validators.required        
      ])),
      senha: new FormControl(usuario.senha, Validators.compose([
        Validators.required        
      ])),
      data_nascimento: new FormControl(usuario.data, Validators.compose([
        Validators.required,      
      ])),
      endereco: new FormControl(usuario.endereco, Validators.compose([
        Validators.required        
      ])),
      end_numero: new FormControl(usuario.end_numero, Validators.compose([
        Validators.required        
      ])),
      cep: new FormControl(usuario.cep, Validators.compose([
        Validators.required        
      ])),
      complemento: new FormControl(usuario.complemento, Validators.compose([
        Validators.required        
      ])),
      email: new FormControl(usuario.email, Validators.compose([
        Validators.required        
      ]))
    })
  }

  alterarUsuario(usuario) {
    let showLoading = this.showLoading('Salvando perfil...');
    showLoading.present();
    this.usuarioProvider.editarUsuario(usuario)
    .subscribe(data => {
      localStorage.setItem('usuario', data);
      showLoading.dismiss();
    }, err => {
      let toast = this.toastCtrl.create({
        message: 'Erro ao salvar as informações.',
        duration: 2000,
        position: 'bottom'
      });
      toast.present();
    })
  }

  private showLoading(message) {
    return this.loadingCtrl.create({
      content: message
    });
  }

}
