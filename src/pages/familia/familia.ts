import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { ParenteProvider } from '../../providers/parente/parente';

@IonicPage()
@Component({
  selector: 'page-familia',
  templateUrl: 'familia.html',
})
export class FamiliaPage {
  public familia: any[];
  constructor(public navCtrl: NavController, public navParams: NavParams, private modal: ModalController,
  public parenteProvider: ParenteProvider, public toastCtrl: ToastController, public loadingCtrl: LoadingController) {
    let showLoading = this.showLoading("Carregando informações...");
    showLoading.present();
    this.familia = [{Nome: "Joao Alexandre", Vacinas: 4, Parentesco: 'Filha'}, {Nome: "Mariana", Vacinas: 2, Parentesco: 'Filho'}];
    let usuario = JSON.parse(localStorage.getItem('usuario'));
    this.parenteProvider.getParentes(usuario.email)
    .subscribe(data => {
      this.familia = data;
      showLoading.dismiss();
    }, err => {
      let toast = this.toastCtrl.create({
        message: 'Erro ao carregar as informações.',
        duration: 2000,
        position: 'bottom'
      });
      toast.present();
    })
  }

  updateParente(pessoa) {
    const modal = this.modal.create('ModalParentePage', {data: pessoa});
    
    modal.present();

    modal.onWillDismiss(data => {
      let cont = 0;
      let parente = this.familia.find(parente => {
        cont++;
        return parente.cpf == data.data.cpf
      });
      this.familia.splice(cont-1, 1, data.data);
    });
  }

  addParente(){
    const modal = this.modal.create('ModalParentePage');
    
    modal.present();

    modal.onWillDismiss(data => {
      this.familia.push(data.data);
    });
  }

  toggleAccordion(pessoa) {
    pessoa.Exapanded = !pessoa.Exapanded;
  }

  private showLoading(message) {
    return this.loadingCtrl.create({
      content: message
    });
  }

}
