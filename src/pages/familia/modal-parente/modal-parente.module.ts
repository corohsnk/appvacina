import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalParentePage } from './modal-parente';

@NgModule({
  declarations: [
    ModalParentePage,
  ],
  imports: [
    IonicPageModule.forChild(ModalParentePage),
  ],
})
export class ModalParentePageModule {}
