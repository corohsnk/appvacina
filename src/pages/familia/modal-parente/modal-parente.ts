import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController,ToastController, LoadingController } from 'ionic-angular';
import { ParenteProvider } from '../../../providers/parente/parente';
import { UUID } from 'angular2-uuid';
@IonicPage()
@Component({
  selector: 'page-modal-parente',
  templateUrl: 'modal-parente.html',
})
export class ModalParentePage {

  public parente: any;
  public titulo: any;
  public test: any;
  constructor(private navParams: NavParams, private view: ViewController, public parenteProvider: ParenteProvider,
    public toastCtrl: ToastController, public loadingCtrl: LoadingController) {
    const data = this.navParams.get('data');
    
    if (data !== undefined) {
      this.parente = this.navParams.get('data');
      this.titulo = 'Editar Parente';
    } else {
      this.titulo = 'Novo Parente';
      this.parente = {id: null, email: null, parentesco: null, nome: null, cpf: null};
    }
    this.test = "dsdsdsa";
  }  

  salvarParente() {
    let showLoading = this.showLoading('Salvando parente...');
    showLoading.present();
    let uuid = UUID.UUID();
    this.parente.id = uuid;
    const usuario = JSON.parse(localStorage.getItem('usuario'));
    this.parente.email = usuario.email;
    this.parenteProvider.salvarParente(this.parente)
    .subscribe(data => {
      showLoading.dismiss();
      this.fecharModal();
    }, err => {
      console.log(err);
      showLoading.dismiss();
      let toast = this.toastCtrl.create({
        message: 'Erro ao salvar o parente',
        duration: 2000,
        position: 'bottom'
      });
      toast.present();
    })
  }

  updateParente() {
    let showLoading = this.showLoading('Salvando parente...');
    showLoading.present();
    this.parenteProvider.alterarParente(this.parente)
    .subscribe(data => {
      showLoading.dismiss();
      this.fecharModal();
    }, err => {
      showLoading.dismiss();
      let toast = this.toastCtrl.create({
        message: 'Erro ao editar o parente.',
        duration: 2000,
        position: 'bottom'
      });
      toast.present();
    })
  }
  
  fecharModal() {
    this.view.dismiss({data: this.parente})
  }

  private showLoading(message) {
    return this.loadingCtrl.create({
      content: message
    });
  }

}
