import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HomePage } from '../home/home';
import { MenuController } from 'ionic-angular';
import { UsuarioProvider } from '../../providers/usuario/usuario';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public formLogin;
  public loading: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public menu: MenuController, public usuarioProvider: UsuarioProvider,
  public loadingCtrl: LoadingController, public toastController: ToastController) {
    this.loading = false;
    this.menu.enable(false);
    this.formLogin = new FormGroup({
      email: new FormControl("corohsnk@hotmail.com", Validators.compose([
        Validators.required        
      ])),
      senha: new FormControl("12312312", Validators.compose([
        Validators.required        
      ]))
    })
  }

  login(usuario) {
    let showLoading = this.showLoading('Autenticando...');
    showLoading.present();
    this.usuarioProvider.logar(usuario)
    .subscribe( data => {
      localStorage.setItem('usuario', JSON.stringify(data));
      this.navCtrl.setRoot(HomePage);
      showLoading.dismiss();
    }, err => {
      console.log(err);
      showLoading.dismiss();
      let toast = this.toastController.create({
        message: 'Usuario ou senha incorreto',
        duration: 2000,
        position: 'bottom'
      });
      toast.present();
      
    })
    
  }

  registrar() {
    this.navCtrl.push(RegisterPage);
  }

  private showLoading(message) {
    return this.loadingCtrl.create({
      content: message
    });
  }


}
