import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddParentePage } from './add-parente';

@NgModule({
  declarations: [
    AddParentePage,
  ],
  imports: [
    IonicPageModule.forChild(AddParentePage),
  ],
})
export class AddParentePageModule {}
