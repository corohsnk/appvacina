import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LocalidadeInfoPage } from './localidade-info';

@NgModule({
  declarations: [
    LocalidadeInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(LocalidadeInfoPage),
  ],
})
export class LocalidadeInfoPageModule {}
