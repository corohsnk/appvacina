import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public vacinas: any[];
  constructor(public navCtrl: NavController, public menu: MenuController) {
    this.menu.enable(true);
    this.vacinas = [
      {Nome: "Gripe", Descricao: "BlaBlaBlaBla", Data: "30/10/2017", valido: false},
      {Nome: "Sarampo", Descricao: "Hheheheh", Data: "10/12/2017", valido: true},
      {Nome: "Tríplice Viral", Descricao: "Sarampo, caxumba e rubéola", Data: "30/12/2017", valido: true}
    ]
  }

}
